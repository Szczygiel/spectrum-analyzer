/*
 *	\file	LCD_5110.c
 *	\brief	Funkcje do obs�ugi wy�wietlacza LCD z NOKII 5110
 *
 *  Created on: 22.06.2018
 *      Author: Michal_Szczygiel
 */
#include "stm32f1xx_hal.h"
#include "stm32f1xx_it.h"
#include "string.h"
#include <../App/LCD_5110.h>
#include <../App/Font.h>
#include <../inc/main.h>

extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim1;
extern UART_HandleTypeDef huart3;

uint8_t LCDBuffer[LCD_BUFFER_SIZE];

void LCD_Init()
{
	LCD_RST;

	LCD_Cmd(LCD5110_FUNCTION_SET | LCD5110_EXTENDED_INSTRUCTION_SET);	// Function set - extended instruction set
	LCD_Cmd(LCD5110_BIAS_SYSTEM | 0x04);								// Bias - 1:48
	LCD_Cmd(LCD5110_SET_VOP | 0x2f);									// Ustawienie kontrastu
	LCD_Cmd(LCD5110_FUNCTION_SET | 0x00);								// Function set - basic instruction set, horizontal addressing
	LCD_Cmd(LCD5110_DISPLAY_CONTROL | LCD5110_DISPLAY_NORMAL_MODE);		// Display control - normal mode
	LCD_Cmd(LCD5110_X_ADDRESS | 0x00);
}

void LCD_Cmd(uint8_t cmd)
{
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(LCD_D_C_GPIO_Port, LCD_D_C_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, &cmd, 1, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(LCD_D_C_GPIO_Port, LCD_D_C_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET);
}

void LCD_Clear(uint8_t *LCDBuffer)
{
	memset(LCDBuffer, 0x00, (uint16_t)LCD_BUFFER_SIZE);
}

void LCD_Frame(uint8_t *LCDBuffer, uint8_t *data, uint16_t size)
{
	memcpy(LCDBuffer, data, (uint16_t)LCD_BUFFER_SIZE);
}

void LCD_Data(uint8_t *LCDBuffer, uint16_t size)
{
	HAL_GPIO_WritePin(LCD_D_C_GPIO_Port, LCD_D_C_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&hspi1, (uint8_t*)LCDBuffer, size, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET);
}

//
//	Wy�wietlenie tekstu w jednej linii wy�wietlacza
//
void LCD_Text_5x7_Left(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text)
{
	uint8_t i;						// kolumna litery do wy�wietlenia
	uint8_t l;						// numer kolumny w wierszu wy�wietlacza
	const uint8_t *font;			// wska�nik na wy�wietlan� liter�
	LCD_Buffer += row * 84 + col;	// pocz�tek wiersza
	l = col;

	while (*text)
	{
		font = &font_ASCII_5x7[*text++ - ' '][0];
		for (i = 0; i < 5; i++)
		{
			*LCD_Buffer++ = *font++;
			if(++l == 84) break;	// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
		}
		*LCD_Buffer++ = 0;			// dodanie pustej kolumny za znakiem dla polepszenia czytelno�ci
		if(++l == 84) break;		// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
	}
}

void LCD_Text_5x7_Center(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text)
{
	uint8_t i;						// Kolumna litery do wy�wietlenia
	uint8_t l = col;				// Numer kolumny w wierszu wy�wietlacza
	uint8_t length_pix = 0;			// D�ugo�c wyswietlanego tekstu w znakach
	uint8_t length_char = 0;		// D�ugo�c wyswietlanego tekstu w znakach

	const uint8_t *font;			// Wska�nik na wy�wietlan� liter�
	const char *tmp = text;

	while (*text++) {};				// Wyszukanie ko�ca tekstu

	length_char = text - tmp - 1;		// D�ugo�c tekstu do wy�wietlenia w znakach
	length_pix = length_char*(5+1);		// D�ugo�c tekstu do wy�wietlenia w punktach
	LCD_Buffer += row * 84 + (84 - length_pix)/2;	// Pocz�tek wiersza do wysrodkowania

	text = tmp;
	while (*text)
	{
		font = &font_ASCII_5x7[*text++ - ' '][0];
		for (i = 0; i < 5; i++)
		{
			*LCD_Buffer++ = *font++;
			if(++l == 84) break;	// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
		}
		*LCD_Buffer++ = 0;			// dodanie pustej kolumny za znakiem dla polepszenia czytelno�ci
		if(++l == 84) break;		// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
	}
}

void LCD_Text_5x7_Right(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text)
{
	uint8_t i;						// Kolumna litery do wy�wietlenia
	uint8_t l = col;				// Numer kolumny w wierszu wy�wietlacza
	uint8_t length_pix = 0;			// D�ugo�c wyswietlanego tekstu w znakach
	uint8_t length_char = 0;		// D�ugo�c wyswietlanego tekstu w znakach

	const uint8_t *font;			// Wska�nik na wy�wietlan� liter�
	const char *tmp = text;

	while (*text++) {};				// Wyszukanie ko�ca tekstu

	length_char = text - tmp - 1;		// D�ugo�c tekstu do wy�wietlenia w znakach
	length_pix = length_char*(5+1);		// D�ugo�c tekstu do wy�wietlenia w punktach
	LCD_Buffer += row * 84 + 84 - length_pix + 1;	// Pocz�tek wiersza do wysrodkowania

	text = tmp;
	while (*text)
	{
		font = &font_ASCII_5x7[*text++ - ' '][0];
		for (i = 0; i < 5; i++)
		{
			*LCD_Buffer++ = *font++;
			if(++l == 84) break;	// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
		}
		*LCD_Buffer++ = 0;			// dodanie pustej kolumny za znakiem dla polepszenia czytelno�ci
		if(++l == 84) break;		// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
	}
}

void LCD_Text_5x7(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text, uint8_t mask)
{
	uint8_t i;						// Kolumna litery do wy�wietlenia
	uint8_t l = col;				// Numer kolumny w wierszu wy�wietlacza
	uint8_t length_pix = 0;			// D�ugo�c wyswietlanego tekstu w znakach
	uint8_t length_char = 0;		// D�ugo�c wyswietlanego tekstu w znakach
	const uint8_t *font;			// Wska�nik na wy�wietlan� liter�
	const char *tmp = text;

	if(row<LCD_HIGH || col<LCD_WIDTH)
	{
		while (*text++) {};													// Wyszukanie ko�ca tekstu
		length_char = text - tmp - 1;										// D�ugo�c tekstu do wy�wietlenia w znakach

		switch(mask & TEXT_MASK)
		{
			case TEXT_LEFT:
				LCD_Buffer += row * LCD_WIDTH + col;						// pocz�tek wiersza
				break;
			case TEXT_RIGHT:
				length_pix = length_char*(5+1);								// D�ugo�c tekstu do wy�wietlenia w punktach
				LCD_Buffer += row * LCD_WIDTH + (LCD_WIDTH - length_pix);	// Pocz�tek wiersza do wysrodkowania
				break;
			case TEXT_CENTER:
				length_pix = length_char*(5+1);								// D�ugo�c tekstu do wy�wietlenia w punktach
				LCD_Buffer += row * LCD_WIDTH + (LCD_WIDTH - length_pix)/2;	// Pocz�tek wiersza do wysrodkowania
				break;
			default:														// wyr�wnanie do lewej
				LCD_Buffer += row * LCD_WIDTH + col;						// pocz�tek wiersza
				break;
		}

		text = tmp;
		while(*text)
		{
			if(*text < 0x80)
			{
				font = &font_ASCII_5x7[*text - ' '][0];
			}
			else
			{
				i=0;
				while(i<18)
				{
					if(*text == PL_font_tab[i])
						break;
					i++;
				}

				font = &font_ASCII_5x7[0x80 + i - ' '][0];
			}

			text++;

			for (i = 0; i < 5; i++)
			{
				*LCD_Buffer++ = (((mask&TEXT_NEGATIVE) == TEXT_NEGATIVE) ? (~(*font++)) : (*font++));
				if(++l == 84) break;	// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
			}
			*LCD_Buffer++ = (((mask&TEXT_NEGATIVE) == TEXT_NEGATIVE) ? 0xff : 0x00);			// dodanie pustej kolumny za znakiem dla polepszenia czytelno�ci
			if(++l == 84) break;		// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
		}
	}
}

void LCD_Text_16x16(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text, uint8_t mask)
{
	uint8_t i;						// Kolumna litery do wy�wietlenia
	uint8_t l = col;				// Numer kolumny w wierszu wy�wietlacza
	uint8_t length_pix = 0;			// D�ugo�c wyswietlanego tekstu w znakach
	uint8_t length_char = 0;		// D�ugo�c wyswietlanego tekstu w znakach
	const uint8_t *font;			// Wska�nik na wy�wietlan� liter�
	const char *tmp = text;

	if(row<LCD_HIGH || col<LCD_WIDTH)
	{
		while (*text++) {};													// Wyszukanie ko�ca tekstu
		length_char = text - tmp - 1;										// D�ugo�c tekstu do wy�wietlenia w znakach

		if(length_char>6)
			length_char = 6;

		switch(mask & TEXT_MASK)
		{
		case TEXT_LEFT:
			LCD_Buffer += row * LCD_WIDTH + col;						// pocz�tek wiersza
			break;
		case TEXT_RIGHT:
			length_pix = length_char*12;								// D�ugo�c tekstu do wy�wietlenia w punktach
			LCD_Buffer += row * LCD_WIDTH + (LCD_WIDTH - length_pix);	// Pocz�tek wiersza do wysrodkowania
			break;
		case TEXT_CENTER:
			length_pix = length_char*12;								// D�ugo�c tekstu do wy�wietlenia w punktach
			LCD_Buffer += row * LCD_WIDTH + (LCD_WIDTH - length_pix)/2;	// Pocz�tek wiersza do wysrodkowania
			break;
		default:														// wyr�wnanie do lewej
			LCD_Buffer += row * LCD_WIDTH + col;						// pocz�tek wiersza
			break;
		}

		text = tmp;
		while(*text && length_char--)
		{
			if(*text < 0x80)
			{
				font = &font_ASCII_16x16[*text - ' '][0];
			}
			else
			{
				i=0;
				while(i<18)
				{
					if(*text == PL_font_tab[i])
						break;
					i++;
				}
				font = &font_ASCII_16x16[0x80 + i - ' '][0];
			}

			text++;

			for (i = 0; i < 12; i++)
			{
				*LCD_Buffer = (((mask&TEXT_NEGATIVE) == TEXT_NEGATIVE) ? (~(*font)) : (*font));
				*(LCD_Buffer+LCD_WIDTH) = (((mask&TEXT_NEGATIVE) == TEXT_NEGATIVE) ? (~(*(font+12))) : (*(font+12)));
				font++;
				LCD_Buffer++;
				if(++l == 84) break;	// przerwanie wy�wietlania gdy dojdziemy do ko�ca wiersza LCD
			}
		}
	}

}


void LCD_Set_Pixel(uint8_t *LCD_Buffer, int x, int y)
{
	LCD_Buffer[ x + (y >> 3) * LCD_WIDTH] |= 1 << (y & 7);
}

void LCD_Draw_Line(uint8_t *LCD_Buffer, int x1, int y1, int x2, int y2)
{
	int dx, dy, sx, sy;

	if (x2 >= x1)
	{
		dx = x2 - x1;
		sx = 1;
	}
	else
	{
		dx = x1 - x2;
		sx = -1;
	}
	if (y2 >= y1)
	{
		dy = y1 - y2;
		sy = 1;
	}
	else
	{
		dy = y2 - y1;
		sy = -1;
	}

	int dx2 = dx << 1;
	int dy2 = dy << 1;
	int err = dx2 + dy2;

	while (1)
	{
		LCD_Set_Pixel(LCD_Buffer, x1, y1);
		if (err >= dy)
		{
			if (x1 == x2)
				break;
			err += dy2;
			x1 += sx;
		}
		if (err <= dx)
		{
			if (y1 == y2)
				break;
			err += dx2;
			y1 += sy;
		}
	}
}

void LCD_Draw_Box(uint8_t *LCD_Buffer, int x1, int y1, int x2, int y2)
{
	LCD_Draw_Line(LCD_Buffer, x1, y1, x1+(x2-x1), y1);
	LCD_Draw_Line(LCD_Buffer, x1, y1, x1, y1+(y2-y1));
	LCD_Draw_Line(LCD_Buffer, x1, y2, x1+(x2-x1), y2);
	LCD_Draw_Line(LCD_Buffer, x2, y1, x2, y1+(y2-y1));
}

