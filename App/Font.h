/*
 * font.h
 *
 *  Created on: 03.10.2018
 *      Author: Michal_Szczygiel
 */

#ifndef FONT_H_
#define FONT_H_

#include <stdint.h>

extern const uint8_t font_ASCII_5x7[][5];
extern const uint8_t font_ASCII_5x8[][5];
extern const uint8_t font_ASCII_16x16[][24];
extern const uint8_t PL_font_tab[];
extern const uint8_t logo_szczygiel[];

#endif /* FONT_H_ */
