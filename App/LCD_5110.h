/*
 * LCD_5110.h
 *
 *  Created on: 22.06.2018
 *      Author: Michal_Szczygiel
 */
#include "stm32l0xx_hal.h"
#include "main.h"

#ifndef LCD_5110_H_
#define LCD_5110_H_

//
//	Funkcje
//
#define LCD5110_NOP							0x00
#define LCD5110_FUNCTION_SET				0x20

//
//	Basic instruction set H=0
//
#define LCD5110_DISPLAY_CONTROL				0x08
#define LCD5110_Y_ADDRESS					0x40
#define LCD5110_X_ADDRESS					0x80
//
//	Extended instruction set H=1
//
#define LCD5110_TEMPERATURE_CONTROL			0x04
#define LCD5110_BIAS_SYSTEM					0x10
#define LCD5110_SET_VOP						0x80

#define LCD5110_POWER_DOWN_BIT				0x04
#define LCD5110_VERTICAL_ADDRESSING_BIT		0x02
#define LCD5110_HORIZONTAL_ADDRESSING_BIT	0x00
#define LCD5110_EXTENDED_INSTRUCTION_SET	0x01
#define LCD5110_DISPLAY_BLANK				0x00
#define LCD5110_DISPLAY_NORMAL_MODE			0x04
#define LCD5110_DISPLAY_ALL_ON				0x01
#define LCD5110_DISPLAY_INVERS_MODE			0x04 | 0x01

#define LCD_WIDTH							84
#define LCD_HIGH							6
#define LCD_BUFFER_SIZE						(84 * 48 / 8)

#define LCD_RST	HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_RESET); \
				HAL_Delay(10); \
				HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_SET);

enum TEXT_STYLE{TEXT_LEFT, TEXT_RIGHT, TEXT_CENTER, TEXT_MASK, TEXT_NEGATIVE};

enum
{
	LCD_CMD_INIT = 0,
	LCD_CMD_WRITE_5X7_TXT_LEFT,
	LCD_CMD_WRITE_5X7_TXT_RIGHT,
	LCD_CMD_WRITE_5X7_TXT_CENTER,
	LCD_CMD_WRITE_16X16_TXT,
	LCD_CMD_WRITE_GRAFIC,
	LCD_CMD_DRAW_PIXEL,
	LCD_CMD_DRAW_LINE,
	LCD_CMD_DRAW_BOX,
}flashCmd;

void LCD_Init();
void LCD_Cmd(uint8_t cmd);
void LCD_Data(uint8_t *LCDBuffer, uint16_t size);
void LCD_Frame(uint8_t *LCDBuffer, uint8_t *data, uint16_t size);

void LCD_Clear();
void LCD_Text_5x7_Left(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text);
void LCD_Text_5x7_Center(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text);
void LCD_Text_5x7_Right(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text);
void LCD_Text_5x7(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text, uint8_t mask);
void LCD_Text_16x16(uint8_t *LCD_Buffer, uint8_t row, uint8_t col, const char* text, uint8_t mask);
void LCD_Set_Pixel(uint8_t *LCD_Buffer, int x, int y);
void LCD_Draw_Line(uint8_t *LCD_Buffer, int x1, int y1, int x2, int y2);
void LCD_Draw_Box(uint8_t *LCD_Buffer, int x1, int y1, int x2, int y2);

extern uint8_t LCDBuffer[LCD_BUFFER_SIZE];

#endif /* LCD_5110_H_ */
