/**
 ****************************************************************************************************
 	@file			circularBuffer.h
 	@author			Micha� Szczygie�
 	$Contact:		michal.szczygiel@zeg-energetyka.pl$
 	$MCU:			STM32F207$
 	$IDE:			Atollic TrueSTUDIO for STM32 v9.3.0$
 	@version		v0.1
 	$Revision: 		v0.0.1$
 	@date			28.10.2019r.
 	@brief			Funkcje obs�ugi bufora ko�owego - plik nag��wkowy
 	@copyright		ZEG-ENERGETYKA
 ****************************************************************************************************
*/

#ifndef __CIRCULARBUFFER_H
#define __CIRCULARBUFFER_H

#include <stdio.h>

#define CIRCULAR_BUFFER_SIZE 128 + 4
#define CIRCULAR_SPI_BUFFER_SIZE 256 + 4

typedef struct CircularBuffer
{
	uint8_t *circularBuffer;
	uint16_t head;
	uint16_t tail;
	uint16_t size;
	uint8_t empty;
}circularBuffer_t;

void CircularBuffer_Init(circularBuffer_t *localCircular, uint8_t *buffer, uint16_t size);
uint8_t CircularBuffer_Put(circularBuffer_t *localCircular, uint8_t data);
uint8_t CircularBuffer_Get(circularBuffer_t *localCircular, uint8_t *data);
uint16_t CircularBuffer_Check(circularBuffer_t *localCircular);
uint8_t CircularBuffer_Put_Frame(circularBuffer_t *localCircular, uint8_t *data, uint16_t numbers);
uint8_t CircularBuffer_Get_Frame(circularBuffer_t *localCircular, uint8_t *data, uint16_t *len);
void CircularBuffer_Flush(circularBuffer_t *localCircular);

#endif /* __CIRCULARBUFFER_H */
