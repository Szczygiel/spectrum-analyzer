/*
 * LED.h
 *
 *  Created on: 24.07.2019
 *      Author: mszczygiel
 */

#ifndef LED_H
#define LED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#define GPIO_NUMBER           (16U)

#define LED_SYSTEM_ON 			LED_GREEN_GPIO_Port->BSRR = LED_GREEN_Pin
#define LED_SYSTEM_OFF 			LED_GREEN_GPIO_Port->BRR = LED_GREEN_Pin
#define LED_SYSTEM_TOGGLE 		LED_GREEN_GPIO_Port->BSRR = ((LED_GREEN_GPIO_Port->ODR & LED_GREEN_Pin) << GPIO_NUMBER) | (~(LED_GREEN_GPIO_Port->ODR) & LED_GREEN_Pin)
#ifdef __cplusplus
}
#endif
#endif /* LED_H */
