/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LED.h"
#include "CircularBuffer.h"
#include "../../app/LCD_5110.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId txUartTaskHandle;
osThreadId rxUartTaskHandle;
osThreadId SpiTaskHandle;
osThreadId I2CTaskHandle;
osThreadId LedTaskHandle;
osThreadId buttonTaskHandle;
osThreadId LcdTaskHandle;
osMessageQId LcdQueueHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
   
/* USER CODE END FunctionPrototypes */

void defaultTaskFun(void const * argument);
void txUartTaskFun(void const * argument);
void rxUartTaskFun(void const * argument);
void SpiTaskFun(void const * argument);
void I2CTaskFun(void const * argument);
void LedTaskFun(void const * argument);
void buttonTaskFun(void const * argument);
void LcdTaskFunk(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* GetTimerTaskMemory prototype (linked to static allocation support) */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];
  
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}                   
/* USER CODE END GET_IDLE_TASK_MEMORY */

/* USER CODE BEGIN GET_TIMER_TASK_MEMORY */
static StaticTask_t xTimerTaskTCBBuffer;
static StackType_t xTimerStack[configTIMER_TASK_STACK_DEPTH];
  
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize )  
{
  *ppxTimerTaskTCBBuffer = &xTimerTaskTCBBuffer;
  *ppxTimerTaskStackBuffer = &xTimerStack[0];
  *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
  /* place for user code */
}                   
/* USER CODE END GET_TIMER_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
       
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of LcdQueue */
  osMessageQDef(LcdQueue, 32, uint8_t);
  LcdQueueHandle = osMessageCreate(osMessageQ(LcdQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, defaultTaskFun, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of txUartTask */
  osThreadDef(txUartTask, txUartTaskFun, osPriorityIdle, 0, 128);
  txUartTaskHandle = osThreadCreate(osThread(txUartTask), NULL);

  /* definition and creation of rxUartTask */
  osThreadDef(rxUartTask, rxUartTaskFun, osPriorityIdle, 0, 128);
  rxUartTaskHandle = osThreadCreate(osThread(rxUartTask), NULL);

  /* definition and creation of SpiTask */
  osThreadDef(SpiTask, SpiTaskFun, osPriorityIdle, 0, 128);
  SpiTaskHandle = osThreadCreate(osThread(SpiTask), NULL);

  /* definition and creation of I2CTask */
  osThreadDef(I2CTask, I2CTaskFun, osPriorityIdle, 0, 128);
  I2CTaskHandle = osThreadCreate(osThread(I2CTask), NULL);

  /* definition and creation of LedTask */
  osThreadDef(LedTask, LedTaskFun, osPriorityIdle, 0, 64);
  LedTaskHandle = osThreadCreate(osThread(LedTask), NULL);

  /* definition and creation of buttonTask */
  osThreadDef(buttonTask, buttonTaskFun, osPriorityIdle, 0, 64);
  buttonTaskHandle = osThreadCreate(osThread(buttonTask), NULL);

  /* definition and creation of LcdTask */
  osThreadDef(LcdTask, LcdTaskFunk, osPriorityIdle, 0, 64);
  LcdTaskHandle = osThreadCreate(osThread(LcdTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_defaultTaskFun */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_defaultTaskFun */
void defaultTaskFun(void const * argument)
{
  /* USER CODE BEGIN defaultTaskFun */
	osMessagePut(LcdQueueHandle, LCD_CMD_INIT, portMAX_DELAY);
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END defaultTaskFun */
}

/* USER CODE BEGIN Header_txUartTaskFun */
/**
* @brief Function implementing the txUartTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_txUartTaskFun */
void txUartTaskFun(void const * argument)
{
  /* USER CODE BEGIN txUartTaskFun */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END txUartTaskFun */
}

/* USER CODE BEGIN Header_rxUartTaskFun */
/**
* @brief Function implementing the rxUartTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_rxUartTaskFun */
void rxUartTaskFun(void const * argument)
{
  /* USER CODE BEGIN rxUartTaskFun */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END rxUartTaskFun */
}

/* USER CODE BEGIN Header_SpiTaskFun */
/**
* @brief Function implementing the SpiTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_SpiTaskFun */
void SpiTaskFun(void const * argument)
{
  /* USER CODE BEGIN SpiTaskFun */
	circularBuffer_t CircularSPI1Receive;
	circularBuffer_t CircularSPI1Transmit;
	uint8_t SPI1BufferTransmit[CIRCULAR_SPI_BUFFER_SIZE];
	uint8_t SPI1BufferReceive[CIRCULAR_SPI_BUFFER_SIZE];

	CircularBuffer_Init(&CircularSPI1Transmit, SPI1BufferTransmit, CIRCULAR_SPI_BUFFER_SIZE);
	CircularBuffer_Init(&CircularSPI1Receive, SPI1BufferReceive, CIRCULAR_SPI_BUFFER_SIZE);

  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END SpiTaskFun */
}

/* USER CODE BEGIN Header_I2CTaskFun */
/**
* @brief Function implementing the I2CTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_I2CTaskFun */
void I2CTaskFun(void const * argument)
{
  /* USER CODE BEGIN I2CTaskFun */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END I2CTaskFun */
}

/* USER CODE BEGIN Header_LedTaskFun */
/**
* @brief Function implementing the LedTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LedTaskFun */
void LedTaskFun(void const * argument)
{
  /* USER CODE BEGIN LedTaskFun */
  /* Infinite loop */
  for(;;)
  {
	  LED_SYSTEM_TOGGLE;
    osDelay(1000);
  }
  /* USER CODE END LedTaskFun */
}

/* USER CODE BEGIN Header_buttonTaskFun */
/**
* @brief Function implementing the buttonTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_buttonTaskFun */
void buttonTaskFun(void const * argument)
{
  /* USER CODE BEGIN buttonTaskFun */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END buttonTaskFun */
}

/* USER CODE BEGIN Header_LcdTaskFunk */
/**
* @brief Function implementing the LcdTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_LcdTaskFunk */
void LcdTaskFunk(void const * argument)
{
  /* USER CODE BEGIN LcdTaskFunk */
  /* Infinite loop */
	static osEvent evt;

	for (;;)
	{
		evt = osMessageGet(LcdQueueHandle, portMAX_DELAY);
		if(evt.status == osEventMessage)
		{
			switch(evt.value.v)
			{
				case LCD_CMD_INIT:
					break;
				case LCD_CMD_WRITE_5X7_TXT_LEFT:
					break;
				case LCD_CMD_WRITE_5X7_TXT_RIGHT:
					break;
				case LCD_CMD_WRITE_5X7_TXT_CENTER:
					break;
				case LCD_CMD_WRITE_16X16_TXT:
					break;
				case LCD_CMD_WRITE_GRAFIC:
					break;
				case LCD_CMD_DRAW_PIXEL:
					break;
				case LCD_CMD_DRAW_LINE:
					break;
				case LCD_CMD_DRAW_BOX:
					break;
				default:
					break;
			}
		}
	}
  /* USER CODE END LcdTaskFunk */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
     
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
