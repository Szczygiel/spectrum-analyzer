/**
 *******************************************************************************
 	@file			circularBuffer.c
 	@author			Micha� Szczygie�
 	$Contact:		michal.szczygiel@zeg-energetyka.pl$
 	$MCU:			STM32F207$
 	$IDE:			Atollic TrueSTUDIO for STM32 v9.3.0$
 	@version		v0.1
 	$Revision: 		v0.0.1$
 	@date			28.10.2019r.
 	@brief			Funkcje obs�ugi bufora ko�owego
 	@copyright		ZEG-ENERGETYKA
 *******************************************************************************
*/

/* Privet  includes ----------------------------------------------------------*/
#include <stdio.h>
#include "CircularBuffer.h"
/* Privet  define ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private user code ---------------------------------------------------------*/

void CircularBuffer_Init(circularBuffer_t *localCircular, uint8_t *buffer, uint16_t size)
{
	localCircular->circularBuffer = buffer;
	localCircular->head = 0;
	localCircular->tail = 0;
	localCircular->size = size;
}

uint8_t CircularBuffer_Put(circularBuffer_t *localCircular, uint8_t data)
{
	uint16_t status = 0;

	uint16_t head_temp = localCircular->head + 1;	// Przypisujemy do zmiennej nast�pny indeks head

	// Je�li by� to ostatni element tablicy to ustawiamy wska�nik na jej pocz�tek
	if ( head_temp == localCircular->size )
	{
		head_temp = 0;
	}

	// Sprawdzamy czy jest miejsce w buforze.
	// Je�li bufor jest pe�ny to wychodzimy z funkcji i zwracamy b��d.
	if ( head_temp == localCircular->tail )
	{
		status = -1;
	}
	else
	{
		// Je�li jest miejsce w buforze to przechodzimy dalej:
		localCircular->circularBuffer[head_temp] = data;	// Wpisujemy warto�� do bufora
		localCircular->head = head_temp;			// Zapisujemy nowy indeks head
	}

	return status;
}

uint8_t CircularBuffer_Put_Frame(circularBuffer_t *localCircular, uint8_t *data, uint16_t numbers)
{
	uint16_t status = 0;
	volatile uint16_t count = numbers;

	uint16_t head_temp = localCircular->head + 1;	// Przypisujemy do zmiennej nast�pny indeks head

	// Sprawdzamy czy jest miejsce w buforze.
	// Je�li bufor jest pe�ny to wychodzimy z funkcji i zwracamy b��d.
	if ((head_temp == localCircular->tail) || (numbers>(localCircular->size - CircularBuffer_Check(localCircular))) || (numbers>localCircular->size))
	{
		status = -1;
	}
	else
	{
		while(numbers)
		{
			// Je�li by� to ostatni element tablicy to ustawiamy wska�nik na jej pocz�tek
			if ( head_temp == localCircular->size )
			{
				head_temp = 0;
			}

			// Je�li jest miejsce w buforze to przechodzimy dalej:
			localCircular->circularBuffer[head_temp] = *(data++);	// Wpisujemy warto�� do bufora
			localCircular->head = head_temp++;			// Zapisujemy nowy indeks head
			numbers--;
			count--;
		}
	}

	return status;
}

uint8_t CircularBuffer_Get(circularBuffer_t *localCircular, uint8_t *data)
{
	uint16_t status = 0;

	// Sprawdzamy czy w buforze jest co� do odczytania
	// Je�li bufor jest pusty to wychodzimy z funkcji i zwracamy b��d.
	if (localCircular->head == localCircular->tail)
		status = -1;

	// Je�li jest co� do odczytania to przechodzimy dalej:
	localCircular->tail++; // Inkrementujemy indeks tail
	// Je�li by� to ostatni element tablicy to ustawiamy wska�nik na jej pocz�tek
	if (localCircular->tail == localCircular->size)
		localCircular->tail = 0;

	*data = localCircular->circularBuffer[localCircular->tail];		// Odczytujemy warto�� z bufora

	return status;	// wszystko przebieg�o pozytywnie, wi�c zwracamy 0
}

uint8_t CircularBuffer_Get_Frame(circularBuffer_t *localCircular, uint8_t *data, uint16_t *len)
{
	uint16_t status = 0;
	uint16_t i;
	uint16_t size = CircularBuffer_Check(localCircular);

	// Sprawdzamy czy w buforze jest co� do odczytania
	// Je�li bufor jest pusty to wychodzimy z funkcji i zwracamy b��d.
	if (localCircular->head == localCircular->tail)
		status = -1;

	// Je�li jest co� do odczytania to przechodzimy dalej:
	for(i=0; i<size; i++)
	{
		localCircular->tail++; // Inkrementujemy indeks tail
		// Je�li by� to ostatni element tablicy to ustawiamy wska�nik na jej pocz�tek
		if (localCircular->tail == localCircular->size)
			localCircular->tail = 0;

		*data++ = localCircular->circularBuffer[localCircular->tail];		// Odczytujemy warto�� z bufora
	}

	*len = i;

	return status;	// wszystko przebieg�o pozytywnie, wi�c zwracamy 0
}


uint16_t CircularBuffer_Check(circularBuffer_t *localCircular)
{
	uint16_t value;

	if(localCircular->head >= localCircular->tail)
	{
		value = localCircular->head - localCircular->tail;
	}
	else
	{
		value = localCircular->size - (localCircular->tail - localCircular->head);
	}

	return value;
}

void CircularBuffer_Flush(circularBuffer_t *localCircular)
{
	localCircular->head = 0;
	localCircular->tail = 0;
}
